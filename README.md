### Installation

Die Templates des Lehrstuhl sind im R-Paket `udsstat` enthalten.
Es kann ein Template zur Erstellung von Abschlussarbeiten sowie ein Template für Präsentationen genutzt werden.
Das Paket ist nicht über [*CRAN*](https://cran.r-project.org/doc/FAQ/R-FAQ.html#What-is-CRAN_003f) verfügbar 
sondern kann aus einem BitBucket-Repository installiert werden.

Hierzu wird zusätzlich das R-Paket `devtools` benötigt.

```{r}
install.packages("devtools")

devtools::install_bitbucket("anre005/udsstat")
```

Nach der Installation ist das Template in [RStudio](www.rstudio.com) verfügbar.
Öffnen Sie hierzu das Menü **File** --> **New File** --> **R Markdown**.

Nun öffnet sich ein neues Fenster, wählen Sie hier

**From Template** --> **Thesis (Chair of Statistics & Econometrics at Saarland University)**

oder

**From Template** --> **Presentation (Chair of Statistics & Econometrics at Saarland University)**

Sie sollten jetzt den Body der Rmd-Datei (alles nach dem YAML - Header) durch einen 
eigenen Text ersetzen. 
Die pdf-Datei können Sie erzeugen indem Sie den `Knit`- Button in RStudio benutzen.

Wenn Sie RStudio nicht verwenden möchten/können nutzen Sie die `draft()`-Funktion von
`rmarkdown` um eine neue Rmd-Datei basierend auf dem Template zu erzeugen.
Mit Hilfe der `render()` Funktion wird schließlich die pdf-Datei erzeugt.

```{r}
rmarkdown::draft("MyThesis.Rmd", template = "thesis", package = "udsstat")
rmarkdown::render("MyThesis.Rmd")
```

Verwenden Sie einen UTF-8 fähigen Editor (bspw. RStudio). 
Werden in RStudio Umlaute und Sonderzeichen nicht korrekt dargestellt
öffnen Sie die Datei mit dem richtigen Encoding.

**File** --> **Reopen with Encoding** --> **UTF-8 auswählen**

### Windows + Literaturverzeichnis
Damit das Literaturverzeichnis unter Windows korrekt erzeugt wird ist eine Perl-Installation nötig.
Sie können beispielsweise [Strawberry Perl](http://strawberryperl.com/) verwenden.
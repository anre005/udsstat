find_file <- function(template, file) {
  template <- system.file("rmarkdown", "templates", template, file,
                          package = "udsstat")
  if (template == "") {
    stop("Couldn't find template file ", template, "/", file, call. = FALSE)
  }

  template
}

find_resource <- function(template, file) {
  find_file(template, file.path("resources", file))
}

knitr_fun <- function(name) utils::getFromNamespace(name, 'knitr')

output_asis <- knitr_fun('output_asis')

# Call rmarkdown::pdf_documet and mark the return value as inheriting pdf_document
inherit_pdf_document <- function(...) {
  fmt <- rmarkdown::pdf_document(...)
  fmt$inherits <- "pdf_document"
  fmt
}

inherit_beamer_presentation <- function(...) {
  fmt <- rmarkdown::beamer_presentation(...)
  fmt$inherits <- "beamer_presentation"
  fmt
}

# Helper function to create a custom format derived from pdf_document
# that includes a custom LaTeX template and custom CSL definition
pdf_document_format <- function(..., format, template, csl) {

  # base format
  fmt <- inherit_pdf_document(..., template = find_resource(format, template))

  # add csl to pandoc_args
  fmt$pandoc$args <- c(fmt$pandoc$args,
                       "--csl",
                       rmarkdown::pandoc_path_arg(find_resource(format, csl)))


  # return format
  fmt
}
